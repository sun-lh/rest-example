import React, { Component } from 'react';
import { me, getBankAccounts } from './services/api'
import Customer from './components/Customer';
import Header from './components/Header';
import Portfolio from './components/Portfolio'
import {Col, Row} from 'react-bootstrap'


class App extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      customer: null,
      customerId: null,
      bankAccounts: []
    };
    this.getPortfolio = this.getPortfolio.bind(this);
  }


  getPortfolio(customerId) {
    this.setState({customerId: customerId})

    me(customerId).then((customer) => {
     this.setState({customer: customer.data.attributes})
    });

    getBankAccounts(customerId).then((bankAccounts) => {
      this.setState({bankAccounts: bankAccounts.data})
    });

  }

  render() {
    return (
      <div className="container">
        <Row>
          <Col><Header customerChanged={this.getPortfolio}/></Col>
        </Row>
        <Row>
          <Col><Customer customer={this.state.customer} customerId={this.state.customerId} /></Col>
        </Row>
        <Portfolio customerId={this.state.customerId} bankAccounts={this.state.bankAccounts}/>
      </div>
    );
  }
}

export default App;
