import Config from '../config'
import fetch from 'isomorphic-fetch'


export function me(customerId) {
  return get('customers/' + customerId)
}

export function getBankAccounts(customerId) {
  return get('bank/accounts', customerId)
}

export function getTransactions(customerId, bankAccountId) {
  return get('bank/accounts/' + bankAccountId + '/transactions', customerId);
}

function get(path, customerId) {

  let headers = {
    'Content-Type': 'application/json',
    'X-Api-Key': Config.apiKey
  }

  if (customerId) {
    headers['X-Customer-Id'] = customerId
  }

  return new Promise(function (resolve) {
    fetch(Config.apiUrl + path, {
      method: 'get',
      headers: headers
    })
      .then(response => response.json())
      .then((json) => {
        resolve(json)
      });
  });
}

