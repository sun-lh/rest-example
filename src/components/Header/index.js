import React, { Component } from 'react';
import { Jumbotron, Button, FormControl, Form, Col } from 'react-bootstrap';

export default class Header extends Component {

  constructor(props) {
    super(props);

    this.state = {
      customerId: null
    };

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCustomerChange = this.handleCustomerChange.bind(this);
  }

  handleCustomerChange(e) {
    this.setState({
      customerId: e.target.value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.props.customerChanged(this.state.customerId);
  }

  render() {
    return (
      <Jumbotron>
        <h1>Welcome to the REST example</h1>
        <p>Enter your customer ID below to retrieve your portfolio</p>
        <Form horizontal onSubmit={this.handleSubmit}>
          <Col sm={4}>
            <FormControl type="text" name="customerId" placeholder="Enter your customer ID"
                         value={this.props.customerId} onChange={this.handleCustomerChange}/>
          </Col>
          <Button type="submit">Get portfolio</Button>
        </Form>
      </Jumbotron>
    );
  }
}
