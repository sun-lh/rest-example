import React, {Component} from 'react';
import {Panel, Table} from 'react-bootstrap'
import './BankAccount.css'
import {getTransactions} from '../../services/api'
import {Transactions} from "../Transactions/index";

export default class BankAccount extends Component {

  constructor(props) {
    super(props);

    this.state = {
      transactions: []
    }
  }

  componentDidMount() {
    getTransactions(this.props.customerId, this.props.account.id).then((transactions) => {
      this.setState({
        transactions: transactions.data
      })
    })
  }

  render() {

    const account = this.props.account.attributes;

    return (
      <Panel header="Bank Account" className="BankAccountPanel">
        <Table condensed hover fill>
          <tbody>
          <tr>
            <th>Account Name</th><td>{account.title}</td>
          </tr>
          <tr>
            <th>Account Type</th><td>{account.product} - {account.type}</td>
          </tr>
          <tr>
            <th>BSB</th><td>{account.bsb}</td>
          </tr>
          <tr>
            <th>Account Number</th><td>{account.accountNumber}</td>
          </tr>
          <tr>
            <th>Current balance</th><td>${account.currentBalance.toFixed(2)} {account.currency}</td>
          </tr>
          <tr>
            <th>Transactions</th><td></td>
          </tr>
          </tbody>
        </Table>
        <Transactions transactions={this.state.transactions}/>
      </Panel>
    );
  }
}


