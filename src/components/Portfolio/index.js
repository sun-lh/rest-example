import React, {Component} from 'react';
import BankAccount from '../BankAccount'
import {Row} from 'react-bootstrap'


export default class Portfolio extends Component {

  render() {
    return(
      <div>
        {
          this.props.bankAccounts ?
            this.props.bankAccounts.map(account => {
              return <Row key={account.id}><BankAccount key={account.id} customerId={this.props.customerId}
                                                       account={account}/></Row>
            }) : ''
        }
      </div>
    )
  }
}