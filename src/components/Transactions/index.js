import React, {Component} from 'react'
import {Table} from 'react-bootstrap'

export class Transactions extends Component {

  render() {
    if (!this.props.transactions || this.props.transactions.length < 1) return null;
    const nodes = this.props.transactions.map(function(t){
      return (
        <tr key={t.id}>
          <td>{t.attributes.postedDate}</td>
          <td>{t.attributes.amount}</td>
          <td>{t.attributes.memo}</td>
          <td>{t.attributes.type}</td>
          <td>{t.attributes.effect}</td>
        </tr>
      )
    });

    return (
        <Table striped bordered condensed hover>
          <thead>
          <tr>
            <th>Posted Date</th>
            <th>Amount</th>
            <th>Memo</th>
            <th>Type</th>
            <th>Effect</th>
          </tr>
          </thead>
          <tbody>
          {nodes}
          </tbody>
        </Table>
    )
  }
}