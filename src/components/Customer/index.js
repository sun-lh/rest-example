import React, { Component } from 'react';
import {Panel, Table} from 'react-bootstrap'
import './Customer.css';

export default class Customer extends Component {

  render() {
    if (!this.props.customer) {
      return <span/>
    } else {
      const customer = this.props.customer
      return (
        <Panel header="My Details" className="Customer" >
          <Table condensed hover fill>
            <tbody>
            <tr>
              <th>DOB</th><td>{ customer.dateOfBirth }</td>
            </tr>
            <tr>
              <th>Gender</th><td>{ customer.gender }</td>
            </tr>
            <tr>
              <th>Tenure</th><td>{ customer.tenure } years</td>
            </tr>
            <tr>
              <th>Email</th><td>{ customer.email }</td>
            </tr>
            <tr>
              <th>Home phone</th><td>{ customer.homePhone }</td>
            </tr>
            <tr>
              <th>Mobile phone</th><td>{ customer.mobilePhone }</td>
            </tr>
            </tbody>
          </Table>
        </Panel>
      );
    }
  }
}