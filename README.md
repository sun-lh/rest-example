## Open Suncorp Sandbox REST API Example

This project is an example of how to use the Open Suncorp Sandbox REST APIs.

It is implemented in JavaScript with React but is only meant to serve as
 an example. It will retrieve the bank accounts and associated transactions
 for a given customer ID.


## Getting started

### Configuration file (config.js)
Update this file with the API Key you obtained during registration.
You should not need to change the 'apiUrl' property.

```
export default {
  apiKey: 'PXFgeDAtL34HH0Ye1TC422pwqTi7V9jd99BWXd',
  apiUrl: 'https://api.sunhack.suncorp.com.au/'
}
```

### Running the application
From the command line execute the following:
```sh
npm start
```
By default this will start the application at http://localhost:3000

## API Interaction Overview
### Headers
Calling the APIs requires you pass the API key you recieved during registration as a header.

```
{
    'X-Api-Key': 'STMKvuDAtL9W0YuUKe1TC422pwqTi7V9jd85BWXd'
}
```
When interacting with an API for a specific customer identifier, this must also be added as header.
```
{
    'X-Api-Key': 'STMKvuDAtL9W0YuUKe1TC422pwqTi7V9jd85BWXd',
    'X-Customer-Id': '279482b1-fd09-421e-a866-0cde8cea7924'
}
```

### Example implementation
The example implementation of calling the APIs can be found in `api.js`. The API key and base path
are both coming from the `config.js` file. If a customer id is passed, then the appropriate header is added.
```
function get(path, customerId) {

  let headers = {
    'Content-Type': 'application/json',
    'X-Api-Key': Config.apiKey
  }

  if (customerId) {
    headers["X-Customer-Id"] = customerId
  }

  return new Promise(function (resolve) {
    fetch(Config.apiUrl + path, {
      method: 'get',
      headers: headers
    })
      .then(response => response.json())
      .then((json) => {
        resolve(json)
      });
  });
```

After entering a customer ID and requesting their portfolio, the `getBankAccounts' function is called.
```
get('bank/accounts', customerId)
```
From the results of getting the bank accounts, the account ID can be used to get the associated transactions.
```
get('bank/accounts/' + bankAccountId + '/transactions', customerId);
```
Adding more items from the customer portfolio is simply a case of making calls to the appropriate API and adding a presentational component in the `Portfolio\index.js`

